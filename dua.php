<?php
function ubah_huruf($string){

            $konversi = ['a'=>'b', 'b'=>'c', 'c'=>'d', 'd'=>'e', 'e'=>'f', 'f'=>'g', 'g'=>'h', 'h'=>'i', 'i'=>'j', 'j'=>'k', 'k'=>'l', 'l'=>'m', 'm'=>'n', 'n'=>'o', 'o'=>'p', 'p'=>'q', 'q'=>'r', 'r'=>'s', 's'=>'t', 't'=>'u', 'u'=>'v', 'v'=>'w', 'w'=>'x', 'y'=>'z', 'z'=>'a'];
            $stringtoLower    = strtolower($string);
            $arrayStrings = str_split($stringtoLower);
            $new_data   = '';
    
    
            foreach ($arrayStrings as  $value) {
                $new_data .= $konversi[$value]."";
            }
            return "<b>" . $string . "</b> => " . $new_data;
        }
        
// TEST CASES
echo ubah_huruf('wow'); // xpx
echo "<br>";
echo ubah_huruf('developer'); // efwfmpqfs
echo "<br>";
echo ubah_huruf('laravel'); // mbsbwfm
echo "<br>";
echo ubah_huruf('keren'); // lfsfo
echo "<br>";
echo ubah_huruf('semangat'); // tfnbohbu
echo "<br>";
?>