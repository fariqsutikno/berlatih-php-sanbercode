<?php
function tentukan_nilai($number)
{
    if ($number >= 85 && $number <= 100) {
        echo $number . " => Sangat Baik";
    }elseif ($number < 85 && $number >= 70) {
        echo $number . " => Baik";
    }elseif ($number < 70 && $number >= 60) {
        echo $number . " => Cukup";
    }elseif ($number < 60) {
        echo $number . " => Kurang";
    }elseif ($number < 0 || $number > 100){
        echo $number . " => Masukkan nilai yang benar!";
    }
}

//TEST CASES
echo tentukan_nilai(98); //Sangat Baik
echo "<br>";
echo tentukan_nilai(76); //Baik
echo "<br>";
echo tentukan_nilai(67); //Cukup
echo "<br>";
echo tentukan_nilai(43); //Kurang
echo "<br>";
?>